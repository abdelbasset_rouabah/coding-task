# Makefile for Docker Nginx PHP Composer MySQL

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  code-sniff          Check the API with PHP Code Sniffer (PSR2)"
	@echo "  test                Test application"

run:
	@echo "Running local server..."
	@php -S localhost:8080 -t public/

code-sniff:
	@echo "Checking the standard code..."
	@php ./vendor/bin/phpcs -v --standard=PSR2 app/src/

test: code-sniff
	@echo "\n"
	@echo "Running Unit Tests..."
	@php ./vendor/bin/phpunit app/tests/ --colors=always
