## Run project
1- run `composer install` 

2- to run code_sniffer use: 
```sh
php ./vendor/bin/phpcs -v --standard=PSR2 app/src/
```
or
```sh
make code-sniff
```

3- to run tests use: 
```sh
php ./vendor/bin/phpunit app/tests/ --colors=always
```
or
```sh
make code-sniff
```

4- Run and go to `127.0.0.1:8080` using the command:
```sh
php -S localhost:8080 -t public/
```
or
```sh
make run
```