<?php

declare(strict_types=1);

use App\Rules\BuzzRule;
use App\FizzBuzzPlayer;
use App\Rules\FizzRule;

require("../vendor/autoload.php");

// echo "Hello";

$rules = [
    new FizzRule(),
    new BuzzRule(),
];

$fizz_buzz = new FizzBuzzPlayer(...$rules);

$range = range(1, 100);
foreach ($fizz_buzz->play($range) as $output) {
    echo $output;
    echo "</br>";
}