<?php

declare(strict_types=1);

use App\Rules\BuzzRule;
use App\FizzBuzzPlayer;
use App\Rules\FizzRule;
use PHPUnit\Framework\TestCase;

class FizzBuzzPlayerTest extends TestCase
{
    private $fizz_buzz;

    public function setUp() : void
    {
        $rules = [
            new FizzRule(),
            new BuzzRule(),
        ];
        $this->fizz_buzz = new FizzBuzzPlayer(...$rules);
    }

    public function testConstructor() : void
    {
        $this->assertInstanceOf(FizzBuzzPlayer::class, $this->fizz_buzz);
    }

    /**
     * @dataProvider playForDataProvider
     */
    public function testPlayFor(int $numerator, string $expected) : void
    {
        $actual = $this->fizz_buzz->playFor($numerator);
        $this->assertSame($expected, $actual);
    }

    public function playForDataProvider() : array
    {
        return [
            [1, "1"],
            [19, "19"],
            [18, "Fizz"],
            [5, "Buzz"],
            [15, "FizzBuzz"],
            [65, "Buzz"],
        ];
    }

    /**
     * @dataProvider playDataProvider
     */
    public function testPlay(int $start, int $end, array $expected_play)
    {
        $range = range($start, $end);
        $actual = $this->fizz_buzz->play($range);
        $actual = iterator_to_array($actual);
        $this->assertSame($expected_play, $actual);
    }

    public function playDataProvider() : array
    {
        return ["should return until 16" => [
                    1, 16, [
                    "1",
                    "2",
                    "Fizz",
                    "4",
                    "Buzz",
                    "Fizz",
                    "7",
                    "8",
                    "Fizz",
                    "Buzz",
                    "11",
                    "Fizz",
                    "13",
                    "14",
                    "FizzBuzz",
                    "16",
                    ]
                ],
                "should return from -16" => [
                    -16, 0, [
                    "-16",
                    "FizzBuzz",
                    "-14",
                    "-13",
                    "Fizz",
                    "-11",
                    "Buzz",
                    "Fizz",
                    "-8",
                    "-7",
                    "Fizz",
                    "Buzz",
                    "-4",
                    "Fizz",
                    "-2",
                    "-1",
                    "FizzBuzz",
                    ]
                ],
                "should return FizzBuzz" => [
                    0, 0, ["FizzBuzz",]
                ],
        ];
    }

    // /**
    //  * @dataProvider isFizzDataProvider
    //  */
    // public function testIsFizz(int $numerator, bool $expected_is_fizz)
    // {
    //     $actual = $this->fizz_buzz->isFizz($numerator);
    //     $this->assertSame($expected_is_fizz, $actual);
    // }

    // public function isFizzDataProvider() : array
    // {
    //     return [
    //         [1, false],
    //         [2, false],
    //         [17, false],
    //         [3, true],
    //         [9, true],
    //         [42, true],
    //     ];
    // }

    // /**
    //  * @dataProvider isBuzzDataProvider
    //  */
    // public function testIsBuzz(int $numerator, bool $expected_is_buzz)
    // {
    //     $actual = $this->fizz_buzz->isBuzz($numerator);
    //     $this->assertSame($expected_is_buzz, $actual);
    // }

    // public function isBuzzDataProvider() : array
    // {
    //     return [
    //         [1, false],
    //         [19, false],
    //         [87, false],
    //         [5, true],
    //         [15, true],
    //         [60, true],
    //     ];
    // }

    // /**
    //  * @dataProvider getPredicateDataProvider
    //  */
    // public function testGetPredicate(int $numerator, string $expected_predicate)
    // {
    //     $actual = $this->fizz_buzz->getPredicate($numerator);
    //     $this->assertSame($expected_predicate, $actual);
    // }

    // public function getPredicateDataProvider() : array
    // {
    //     return [
    //         [1, "1"],
    //         [19, "19"],
    //         [18, "Fizz"],
    //         [5, "Buzz"],
    //         [15, "FizzBuzz"],
    //         [65, "Buzz"],
    //     ];
    // }
}