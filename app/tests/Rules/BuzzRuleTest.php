<?php

declare(strict_types=1);

use App\IRule;
use App\Rules\BuzzRule;
use PHPUnit\Framework\TestCase;

class BuzzRuleTest extends TestCase
{
    private $rule;

    public function setUp() : void
    {
        $this->rule = new BuzzRule();
    }

    public function testConstructor() : void
    {
        $this->assertInstanceOf(IRule::class, $this->rule);
    }

    /**
     * @dataProvider isConformDataProvider
     */
    public function testIsConform(int $numerator, bool $expectedIsConform) : void
    {
        $actual = $this->rule->isConform($numerator);
        $this->assertSame($actual, $expectedIsConform);
    }

    public function isConformDataProvider() : array
    {
        return [
            [1, false],
            [19, false],
            [87, false],
            [5, true],
            [15, true],
            [60, true],
        ];
    }
}
