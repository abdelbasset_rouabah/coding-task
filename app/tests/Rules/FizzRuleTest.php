<?php

declare(strict_types=1);

use App\IRule;
use App\Rules\FizzRule;
use PHPUnit\Framework\TestCase;

class FizzRuleTest extends TestCase
{
    private $rule;

    public function setUp() : void
    {
        $this->rule = new FizzRule();
    }

    public function testConstructor() : void
    {
        $this->assertInstanceOf(IRule::class, $this->rule);
    }

    public function testGetPredicate() : void
    {
        $actual = $this->rule->getPredicate();
        $this->assertEquals('Fizz', $actual);
    }

    /**
     * @dataProvider isConformDataProvider
     */
    public function testIsConform(int $numerator, bool $expectedIsConform) : void
    {
        $actual = $this->rule->isConform($numerator);
        $this->assertSame($actual, $expectedIsConform);
    }

    public function isConformDataProvider() : array
    {
        return [
            [1, false],
            [2, false],
            [17, false],
            [3, true],
            [9, true],
            [42, true],
        ];
    }
}
