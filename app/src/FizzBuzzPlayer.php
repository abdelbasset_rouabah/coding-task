<?php

declare(strict_types=1);

namespace App;

class FizzBuzzPlayer
{
    private $rules;

    public function __construct(IRule ...$rules)
    {
        $this->rules = $rules;
    }

    public function play(array $range) : \Generator
    {
        foreach ($range as $num) {
            yield $this->playFor($num);
        }
    }

    public function playFor(int $numerator) : string
    {
        $output = "";
        foreach ($this->rules as $rule) {
            if ($rule->isConform($numerator)) {
                $output .= $rule->getPredicate();
            }
        }

        return $output === "" ? (string) $numerator : $output;
    }
}
