<?php

declare(strict_types=1);

namespace App\Rules;

use App\IRule;

class BuzzRule implements IRule
{
    private $predicate = "Buzz";

    public function getPredicate() : string
    {
        return $this->predicate;
    }

    public function isConform(int $numerator) : bool
    {
        return $numerator % 5 === 0;
    }
}
