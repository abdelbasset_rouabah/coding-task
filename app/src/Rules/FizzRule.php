<?php

declare(strict_types=1);

namespace App\Rules;

use App\IRule;

class FizzRule implements IRule
{
    private $predicate = "Fizz";

    public function getPredicate() : string
    {
        return $this->predicate;
    }

    public function isConform(int $numerator) : bool
    {
        return $numerator % 3 === 0;
    }
}
