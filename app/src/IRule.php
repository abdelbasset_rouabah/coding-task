<?php

declare(strict_types=1);

namespace App;

interface IRule
{
    public function getPredicate() : string;
    public function isConform(int $numerator) : bool;
}
